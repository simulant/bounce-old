## Bounce

**Welcome! Bounce is a 3D physics engine for games.**

## Features

### Common

* Efficient data structures with no use of STL
* Frame, stack, and pool allocators
* Built-in math library
* Tunable settings used across the entire library

### Collision

* Dynamic tree broadphase
* Static tree "midphase"
* SAT
* GJK
* Spheres, capsules, convex hulls, triangle meshes 
* Optimized pair management

### Dynamics

* Rigid bodies
* Contact, friction, restitution
* Sphere, cone, revolute, and more joint types
* Joint motors, limits
* Constraint graphs
* Simulation islands and sleep management
* Linear time solver
* Stable shape stacking
* One-shot contact manifolds
* Contact clustering, reduction, and persistence
* Contact callbacks: begin, pre-solve, post-solve
* Ray-casting, convex-casting, and volume queries

### Testbed
	
* OpenGL 2 with GLFW and GLAD
* UI by imgui
* Mouse picking
* CMake build system

### Documentation

* Doxygen API documentation</li>
* [Quickstart Guide](https://github.com/irlanrobson/bounce/blob/master/doc/quickstart_guide.docx)

**Note**: While there isn't a complete user manual available for Bounce, you can use the quickstart guide and the Testbed for learning how to use Bounce. Testbed is a collection of visual tests and examples that can support the development of the library. As you would imagine, this application is not part of the library.

## License

Bounce is released under the [zlib license](https://en.wikipedia.org/wiki/Zlib_License). Please recognize this software in the product documentation if possible.

## Dependencies

### Testbed

These are the external dependencies for the Testbed example project. If you don't care about Testbed, then you don't need these dependencies. 

* [GLFW](https://www.glfw.org/)
* [GLAD](https://glad.dav1d.de/)
* [imgui](https://github.com/ocornut/imgui)

**Note**: There are libraries in the root directory of the project tree that are used across the library. I call them internal dependencies because I wrote them. I haven't researched a better name for them, although probably there exist one.

## Contributing

You can ask anything relative to this project using the issue tracker. 

Please do not open pull requests with bugfixes or new features that require large changes. Open an issue first for discussion. 
