/*
* Copyright (c) 2016-2019 Irlan Robson 
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

#if !defined(profiler_h)

typedef struct profiler_tree_node_stats_t profiler_tree_node_stats_t;
typedef struct profiler_tree_node_t profiler_tree_node_t;

// Profiler tree node
struct profiler_tree_node_t
{
	const char* name; // unique name. used as identifier
	
	float elapsed; // total elapsed time
	size_t call_count; // number of calls inside the parent node
	size_t recursion_call_count; // recursion helper counter
	
	double t0; // internal
	double t1; // internal 
	
	profiler_tree_node_t* parent; // parent node
	profiler_tree_node_t* child_head; // list of children
	profiler_tree_node_t* child_next; // link to the next node in the parent node list of children

	profiler_tree_node_stats_t* stats; // global node statistics
};

// Profiler tree node permanent statistics 
struct profiler_tree_node_stats_t
{
	const char* name; // name
	float min_elapsed; // min elapsed time
	float max_elapsed; // max elapsed time
	profiler_tree_node_stats_t* next; // list into profiler
};

typedef struct profiler_frame_t profiler_frame_t;

// Immediate mode hierarchical profiler 
typedef struct profiler_tree_t
{
	double current_time; // time
	profiler_tree_node_t* root; // root node
	profiler_tree_node_t* top; // top node
	profiler_tree_node_stats_t* stats; // list of permanent statistics
	profiler_frame_t* frame; // frame
} profiler_tree_t;

profiler_tree_t* profiler_tree_create(size_t node_capacity);
void profiler_tree_destroy(profiler_tree_t* tree);

void profiler_tree_begin_frame(profiler_tree_t* tree);
void profiler_tree_end_frame(profiler_tree_t* tree);

void profiler_tree_begin_scope(profiler_tree_t* tree, const char* name);
void profiler_tree_end_scope(profiler_tree_t* tree);

#define profiler_h // #if !defined(profiler_h)

#endif // #if !defined(profiler_h)

#if defined(profiler_implementation)

#if !defined(profiler_implementation_once)
#define profiler_implementation_once

#define profiler_windows 1
#define profiler_mac 2
#define profiler_unix 3

#if defined (_WIN32)
#define profiler_platform profiler_windows
#elif defined( __APPLE__ )
#define profiler_platform profiler_mac
#else
#define profiler_platform profiler_unix
#endif

#if (profiler_platform == profiler_windows)

#include <Windows.h>

float profiler_elapsed_miliseconds()
{
	static int init_bit = 0;
	static LARGE_INTEGER c0;
	static double inv_frequency;

	LARGE_INTEGER c;
	QueryPerformanceCounter(&c);

	if (init_bit == 0)
	{
		c0 = c;

		LARGE_INTEGER frequency;
		QueryPerformanceFrequency(&frequency);

		double cycles_per_second = (double)frequency.QuadPart;
		double seconds_per_cycle = 1.0 / cycles_per_second;
		double miliseconds_per_cycle = 1000.0 * seconds_per_cycle;

		inv_frequency = miliseconds_per_cycle;

		init_bit = 1;
	}

	double dt = inv_frequency * (double)(c.QuadPart - c0.QuadPart);
	c0 = c;
	return (float)dt;
}

#elif (profiler_platform == profiler_mac)

#include <mach/mach_time.h>

float profiler_elapsed_miliseconds()
{
	static int init_bit = 0;
	static uint64_t c0;
	static double inv_frequency;

	uint64_t c = mach_absolute_time();

	if (init_bit = 0)
	{
		c0 = c;

		mach_timebase_info_data_t info;
		mach_timebase_info(&info);
		inv_frequency = (double)(info.numer) / ((double)(info.denom) * 1.0e6);

		init_bit = 1;
	}

	double dt = inv_frequency * (double)(c - c0);
	c0 = c;
	return (float)dt;
}

#else

#include <time.h>

float profiler_elapsed_miliseconds()
{
	static int init_bit = 0;
	static struct timespec c0;
	
	struct timespec c;
	clock_gettime(CLOCK_MONOTONIC, &c);
	
	if (init_bit == 0)
	{
		c0 = c;
		init_bit = 1;
	}
	
	double dt = 1000.0 * double(c.tv_sec - c0.tv_sec) + 1.0e-6 * double(c.tv_nsec - c0.tv_nsec);
	c0 = c;
	return (float)dt;
}

#endif // profiler_platform

#include <assert.h>

#define profiler_ptr_add(ptr, size) ((void*)(((char*)ptr) + (size)))
#define profiler_ptr_sub(ptr, size) ((void*)(((char*)ptr) - (size)))

#if !defined(profiler_alloc_and_free)

#include <stdlib.h> // malloc, free, NULL

#define profiler_alloc(size) malloc(size)
#define profiler_free(p) free(p)

#define profiler_alloc_and_free // #if !defined(profiler_alloc_and_free)

#endif // #if !defined(profiler_alloc_and_free)

float profiler_min_float(float a, float b) { return a < b ? a : b; }
float profiler_max_float(float a, float b) { return a > b ? a : b; }

typedef struct profiler_frame_header_t
{
	size_t size;
	void* p;
	int parent;
} profiler_frame_header_t;

typedef struct profiler_frame_t
{
	size_t capacity;
	void* memory;
	void* ptr;
	size_t allocated_size;
} profiler_frame_t;

void profiler_frame_create(profiler_frame_t* frame, void* memory, size_t size)
{
	frame->capacity = size;
	frame->memory = memory;
	frame->ptr = memory;
	frame->allocated_size = 0;
}

void* profiler_frame_alloc(profiler_frame_t* frame, size_t size)
{
	size_t total_size = sizeof(profiler_frame_header_t) + size;

	if (frame->allocated_size + total_size > frame->capacity)
	{
		void* p = profiler_alloc(total_size);

		profiler_frame_header_t* header = (profiler_frame_header_t*)p;
		header->p = p;
		header->size = size;
		header->parent = 1;
		return profiler_ptr_add(p, sizeof(profiler_frame_header_t));
	}

	void* p = frame->ptr;
	
	frame->ptr = profiler_ptr_add(frame->ptr, total_size);
	frame->allocated_size += total_size;

	profiler_frame_header_t* header = (profiler_frame_header_t*)p;
	header->p = p;
	header->size = size;
	header->parent = 0;
	return profiler_ptr_add(p, sizeof(profiler_frame_header_t));
}

void profiler_frame_free(profiler_frame_t* frame, void* q)
{
	void* p = profiler_ptr_sub(q, sizeof(profiler_frame_header_t));
	profiler_frame_header_t* header = (profiler_frame_header_t*)p;
	assert(header->p == p);
	if (header->parent == 1)
	{
		profiler_free(p);
	}
}

void profiler_frame_reset(profiler_frame_t* frame)
{
	frame->ptr = frame->memory;
	frame->allocated_size = 0;
}

profiler_tree_t* profiler_tree_create(size_t node_capacity)
{
	profiler_tree_t* tree = (profiler_tree_t*)profiler_alloc(sizeof(profiler_tree_t));
	tree->current_time = 0.0;
	tree->root = NULL;
	tree->top = NULL;
	tree->stats = NULL;
	tree->frame = (profiler_frame_t*)profiler_alloc(sizeof(profiler_frame_t));
	
	size_t frame_memory_size = node_capacity * sizeof(profiler_tree_node_t);
	void* frame_memory = profiler_alloc(frame_memory_size);
	profiler_frame_create(tree->frame, frame_memory, frame_memory_size);
	
	return tree;
}

void profiler_tree_destroy(profiler_tree_t* tree)
{
	assert(tree->root == NULL);
	assert(tree->top == NULL);

	profiler_free(tree->frame->memory);
	profiler_free(tree->frame);
	
	profiler_tree_node_stats_t* s = tree->stats;
	while (s)
	{
		profiler_tree_node_stats_t* boom = s;
		s = s->next;
		profiler_free(boom);
	}
	
	profiler_free(tree);
}

profiler_tree_node_t* profiler_tree_find_top_child_node(profiler_tree_t* tree, const char* name)
{
	if (tree->top)
	{
		profiler_tree_node_t* c = tree->top->child_head;
		while (c)
		{
			if (c->name == name)
			{
				return c;
			}
			c = c->child_next;
		}
	}
	return NULL;
}

profiler_tree_node_stats_t* profiler_tree_find_stats(profiler_tree_t* tree, const char* name)
{
	profiler_tree_node_stats_t* s = tree->stats;
	while (s)
	{
		if (s->name == name)
		{
			return s;
		}
		s = s->next;
	}
	return NULL;
}

double profiler_tree_time(profiler_tree_t* tree)
{
	tree->current_time += (double)profiler_elapsed_miliseconds();
	return tree->current_time;
}

void profiler_tree_begin_scope(profiler_tree_t* tree, const char* name)
{
	profiler_tree_node_t* found_top_child_node = profiler_tree_find_top_child_node(tree, name);
	if (found_top_child_node)
	{
		// top child node becomes top node
		tree->top = found_top_child_node;
		
		++found_top_child_node->call_count;
		
		if (found_top_child_node->recursion_call_count == 0)
		{
			found_top_child_node->t0 = profiler_tree_time(tree);
		}

		++found_top_child_node->recursion_call_count;
		return;
	}

	// create a new node
	profiler_tree_node_t* new_node = (profiler_tree_node_t*) profiler_frame_alloc(tree->frame, sizeof(profiler_tree_node_t));
	new_node->name = name;
	new_node->t0 = profiler_tree_time(tree);
	new_node->elapsed = 0.0f;
	new_node->call_count = 1;
	new_node->recursion_call_count = 1;
	new_node->stats = NULL;
	new_node->parent = tree->top;
	new_node->child_head = NULL;
	new_node->child_next = NULL;

	if (tree->root == NULL)
	{
		// insert into tree
		assert(tree->top == NULL);
		tree->root = new_node;
		tree->top = new_node;
		return;
	}

	if (tree->top)
	{
		// top node gets a new kid
		new_node->child_next = tree->top->child_head;
		tree->top->child_head = new_node;
	}

	// new node becomes top node
	tree->top = new_node;
}

void profiler_tree_end_scope(profiler_tree_t* tree)
{
	profiler_tree_node_t* top = tree->top;
	assert(top != NULL);

	--top->recursion_call_count;
	if (top->recursion_call_count > 0)
	{
		return;
	}

	top->t1 = profiler_tree_time(tree);

	float elapsed_time = (float)(top->t1 - top->t0);

	top->elapsed += elapsed_time;

	// permanent statistics
	profiler_tree_node_stats_t* top_stats = profiler_tree_find_stats(tree, top->name);
	if (top_stats == NULL)
	{
		top_stats = (profiler_tree_node_stats_t*)profiler_alloc(sizeof(profiler_tree_node_stats_t));
		top_stats->name = top->name;
		top_stats->min_elapsed = elapsed_time;
		top_stats->max_elapsed = elapsed_time;
		
		// push stat to tree list of stats
		top_stats->next = tree->stats;
		tree->stats = top_stats;
	}
	else
	{
		top_stats->min_elapsed = profiler_min_float(top_stats->min_elapsed, elapsed_time);
		top_stats->max_elapsed = profiler_max_float(top_stats->max_elapsed, elapsed_time);
	}

	if (top->stats == NULL)
	{
		top->stats = top_stats;
	}

	assert(top->stats == top_stats);

	tree->top = top->parent;
}

void profiler_tree_begin_frame(profiler_tree_t* tree)
{
	assert(tree->top == NULL);
	profiler_frame_reset(tree->frame);
}

void profiler_tree_destroy_node_recursively(profiler_tree_t* tree, profiler_tree_node_t* node)
{
	profiler_tree_node_t* c = node->child_head;
	while (c)
	{
		profiler_tree_node_t* boom = c;
		c = c->child_next;
		profiler_tree_destroy_node_recursively(tree, boom);
	}
	profiler_frame_free(tree->frame, node);
}

void profiler_tree_end_frame(profiler_tree_t* tree)
{
	assert(tree->top == NULL);
	if (tree->root)
	{
		profiler_tree_destroy_node_recursively(tree, tree->root);
		tree->root = NULL;
	}
}

#endif // #if !defined(profiler_implementation_once)

#endif //#if defined(profiler_implementation)
