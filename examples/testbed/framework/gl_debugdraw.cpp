/*
* Copyright (c) 2016-2019 Irlan Robson 
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

#include "gl_debugdraw.h"

#include "glad/glad.h"

#define gl_primitive_implementation
#include "gl_primitive.h"

#include "spherecamera.h"

GLDebugDraw::GLDebugDraw(int pointCapacity, int lineCapacity, int triangleCapacity)
{
	m_points = gl_primitive_points_create(pointCapacity);
	m_lines = gl_primitive_lines_create(lineCapacity);
	m_triangles = gl_primitive_triangles_create(triangleCapacity);
	
	glFrontFace(GL_CCW);
	glCullFace(GL_BACK);
	glEnable(GL_CULL_FACE);
	glClearDepth(1.0f);
}

GLDebugDraw::~GLDebugDraw()
{
	gl_primitive_points_destroy(m_points);
	gl_primitive_lines_destroy(m_lines);
	gl_primitive_triangles_destroy(m_triangles);
}

void GLDebugDraw::Begin()
{
	glViewport(0, 0, m_camera->get_width(), m_camera->get_height());	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void GLDebugDraw::End()
{	
}

void GLDebugDraw::SetClearColor(float r, float g, float b, float a)
{
	glClearColor(r, g, b, a);
}

void GLDebugDraw::AddPoint(const b3Vec3& position, const b3Color& color, scalar size)
{
	gl_primitive_points_vertex(m_points, position.x, position.y, position.z, color.r, color.g, color.b, color.a, size);	
}

void GLDebugDraw::AddLine(const b3Vec3& p1, const b3Vec3& p2, const b3Color& color)
{
	gl_primitive_lines_vertex(m_lines, p1.x, p1.y, p1.z, color.r, color.g, color.b, color.a);
	gl_primitive_lines_vertex(m_lines, p2.x, p2.y, p2.z, color.r, color.g, color.b, color.a);	
}

void GLDebugDraw::AddTriangle(const b3Vec3& p1, const b3Vec3& p2, const b3Vec3& p3, const b3Color& color, const b3Vec3& normal)
{
	gl_primitive_triangles_vertex(m_triangles, p1.x, p1.y, p1.z, color.r, color.g, color.b, color.a, normal.x, normal.y, normal.z);
	gl_primitive_triangles_vertex(m_triangles, p2.x, p2.y, p2.z, color.r, color.g, color.b, color.a, normal.x, normal.y, normal.z);
	gl_primitive_triangles_vertex(m_triangles, p3.x, p3.y, p3.z, color.r, color.g, color.b, color.a, normal.x, normal.y, normal.z);
}

void GLDebugDraw::FlushPoints(bool depthEnabled)
{
	if (depthEnabled)
	{
		glEnable(GL_DEPTH_TEST);
	}
	else
	{
		glDisable(GL_DEPTH_TEST);
	}
	
	sc_mat44 P = m_camera->build_projection_matrix();
	sc_mat44 V = m_camera->build_view_matrix();

	sc_mat44 MVP = P * V;
	
	gl_primitive_points_flush(m_points, &MVP.x.x);
}

void GLDebugDraw::FlushLines(bool depthEnabled)
{
	if (depthEnabled)
	{
		glEnable(GL_DEPTH_TEST);
	}
	else
	{
		glDisable(GL_DEPTH_TEST);
	}
	
	sc_mat44 P = m_camera->build_projection_matrix();
	sc_mat44 V = m_camera->build_view_matrix();

	sc_mat44 MVP = P * V;
	
	gl_primitive_lines_flush(m_lines, &MVP.x.x);
}

void GLDebugDraw::FlushTriangles(bool depthEnabled)
{
	if (depthEnabled)
	{
		glEnable(GL_DEPTH_TEST);
	}
	else
	{
		glDisable(GL_DEPTH_TEST);
	}	

	sc_mat44 P = m_camera->build_projection_matrix();
	sc_mat44 V = m_camera->build_view_matrix();

	sc_mat44 MVP = P * V;
		
	gl_primitive_triangles_flush(m_triangles, &MVP.x.x);
}
