/*
* Copyright (c) 2016-2019 Irlan Robson 
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

#if !defined(spheremesh_h)

// This structure represents a generated triangle mesh.
// The vertex normals are the vertices themselves.
typedef struct spheremesh_output_t
{
	int vertex_count; // number of unique vertices
	float* vertices; // list of unique vertices
	int index_count; // number of triangle vertex indices
	int* indices; // list of triangle vertex index
} spheremesh_output_t;

// Return the required size of the required memory buffer passed to spheremesh_generate_mesh.
// This function can return a very large or invalid number if you pass a large number of subdivisions.
int spheremesh_required_bytes(int subdivisions);

// Generate a unit icosphere given the required memory buffer and the number of subdivisions.
// If the number of subdivisions to perform is zero then the output mesh is an octahedron.
spheremesh_output_t spheremesh_generate_mesh(void* required_memory, int subdivisions);

#define spheremesh_h // #if !defined(spheremesh_h)

#endif // #if !defined(spheremesh_h)

#if defined(spheremesh_implementation)

#if !defined(spheremesh_implementation_once)
#define spheremesh_implementation_once

#include <math.h>
#include <stdlib.h>

typedef struct spheremesh_vec3_t
{
	float x;
	float y;
	float z;
} spheremesh_vec3_t;

spheremesh_vec3_t spheremesh_vec3_add(spheremesh_vec3_t a, spheremesh_vec3_t b)
{
	spheremesh_vec3_t c;
	c.x = a.x + b.x;
	c.y = a.y + b.y;
	c.z = a.z + b.z;
	return c;
}

spheremesh_vec3_t spheremesh_vec3_mul(float a, spheremesh_vec3_t b)
{
	spheremesh_vec3_t c;
	c.x = a * b.x;
	c.y = a * b.y;
	c.z = a * b.z;
	return c;
}

spheremesh_vec3_t spheremesh_vec3_normalize(spheremesh_vec3_t a)
{
	float norm = sqrtf(a.x * a.x + a.y * a.y + a.z * a.z);
	float inv_norm = 1.0f / norm;
	return spheremesh_vec3_mul(inv_norm, a);
}

typedef struct spheremesh_mesh_t
{
	int vertex_count;
	spheremesh_vec3_t* vertices;
	int index_count;
	int* indices;
} spheremesh_mesh_t;

void spheremesh_mesh_add_vertex(spheremesh_mesh_t* mesh, float x, float y, float z)
{
	mesh->vertices[mesh->vertex_count].x = x;
	mesh->vertices[mesh->vertex_count].y = y;
	mesh->vertices[mesh->vertex_count].z = z;
	++mesh->vertex_count;
}

void spheremesh_mesh_add_triangle(spheremesh_mesh_t* mesh, int v1, int v2, int v3)
{
	mesh->indices[mesh->index_count++] = v1;
	mesh->indices[mesh->index_count++] = v2;
	mesh->indices[mesh->index_count++] = v3;
}

void spheremesh_mesh_set_octahedron(spheremesh_mesh_t* mesh)
{
	spheremesh_mesh_add_vertex(mesh, 0.0f, -1.0f, 0.0f);
	spheremesh_mesh_add_vertex(mesh, 0.0f, 0.0f, 1.0f);
	spheremesh_mesh_add_vertex(mesh, -1.0f, 0.0f, 0.0f);
	spheremesh_mesh_add_vertex(mesh, 0.0f, 0.0f, -1.0f);
	spheremesh_mesh_add_vertex(mesh, 1.0f, 0.0f, 0.0f);
	spheremesh_mesh_add_vertex(mesh, 0.0f, 1.0f, 0.0f);

	spheremesh_mesh_add_triangle(mesh, 0, 1, 2);
	spheremesh_mesh_add_triangle(mesh, 0, 2, 3);
	spheremesh_mesh_add_triangle(mesh, 0, 3, 4);
	spheremesh_mesh_add_triangle(mesh, 0, 4, 1);

	spheremesh_mesh_add_triangle(mesh, 5, 2, 1);
	spheremesh_mesh_add_triangle(mesh, 5, 3, 2);
	spheremesh_mesh_add_triangle(mesh, 5, 4, 3);
	spheremesh_mesh_add_triangle(mesh, 5, 1, 4);
}

typedef struct spheremesh_edge_t
{
	int v1;
	int v2;
} spheremesh_edge_t;

typedef struct spheremesh_edge_vertex_pair_t
{
	spheremesh_edge_t edge;
	int center_vertex;
} spheremesh_edge_vertex_pair_t;

typedef struct spheremesh_edge_vertex_map_t
{
	int pair_count;
	spheremesh_edge_vertex_pair_t* pairs;
} spheremesh_edge_vertex_map_t;

void spheremesh_edge_vertex_map_add_pair(spheremesh_edge_vertex_map_t* map, spheremesh_edge_vertex_pair_t pair)
{
	map->pairs[map->pair_count++] = pair;
}

// Optimizable.
spheremesh_edge_vertex_pair_t* spheremesh_edge_vertex_map_find(spheremesh_edge_vertex_map_t* map, int v1, int v2)
{
	for (int i = 0; i < map->pair_count; ++i)
	{
		spheremesh_edge_vertex_pair_t* pair = map->pairs + i;
		if (pair->edge.v1 == v1 && pair->edge.v2 == v2)
		{
			return pair;
		}
	}
	return NULL;
}

int spheremesh_add_center_vertex(spheremesh_mesh_t* out_mesh,
	spheremesh_edge_vertex_map_t* map,
	spheremesh_mesh_t* in_mesh,
	int i1, int i2)
{
	spheremesh_edge_vertex_pair_t* pair = spheremesh_edge_vertex_map_find(map, i2, i1);
	if (pair)
	{
		return pair->center_vertex;
	}

	spheremesh_edge_t new_edge;
	new_edge.v1 = i1;
	new_edge.v2 = i2;

	int new_vertex = out_mesh->vertex_count;

	spheremesh_vec3_t v1 = in_mesh->vertices[i1];
	spheremesh_vec3_t v2 = in_mesh->vertices[i2];

	spheremesh_vec3_t center = spheremesh_vec3_mul(0.5f, spheremesh_vec3_add(v1, v2));
	spheremesh_vec3_t normalized_center = spheremesh_vec3_normalize(center);

	spheremesh_mesh_add_vertex(out_mesh, normalized_center.x, normalized_center.y, normalized_center.z);

	spheremesh_edge_vertex_pair_t new_pair;
	new_pair.edge = new_edge;
	new_pair.center_vertex = new_vertex;

	spheremesh_edge_vertex_map_add_pair(map, new_pair);

	return new_vertex;
}

void spheremesh_subdivide_mesh(spheremesh_mesh_t* out_mesh, spheremesh_mesh_t* in_mesh, spheremesh_edge_vertex_map_t* map)
{
	out_mesh->vertex_count = in_mesh->vertex_count;
	for (int i = 0; i < in_mesh->vertex_count; ++i)
	{
		out_mesh->vertices[i] = in_mesh->vertices[i];
	}

	for (int i = 0; i < in_mesh->index_count / 3; ++i)
	{
		int v1 = in_mesh->indices[3 * i + 0];
		int v2 = in_mesh->indices[3 * i + 1];
		int v3 = in_mesh->indices[3 * i + 2];

		int e1v = spheremesh_add_center_vertex(out_mesh, map, in_mesh, v1, v2);
		int e2v = spheremesh_add_center_vertex(out_mesh, map, in_mesh, v2, v3);
		int e3v = spheremesh_add_center_vertex(out_mesh, map, in_mesh, v3, v1);

		spheremesh_mesh_add_triangle(out_mesh, v1, e1v, e3v);
		spheremesh_mesh_add_triangle(out_mesh, e1v, v2, e2v);
		spheremesh_mesh_add_triangle(out_mesh, e2v, v3, e3v);
		spheremesh_mesh_add_triangle(out_mesh, e1v, e2v, e3v);
	}
}

typedef struct spheremesh_required_bytes_t
{
	int in_vertices_bytes;
	int in_indices_bytes;
	int out_vertices_bytes;
	int out_indices_bytes;
	int edge_vertex_pairs_bytes;
} spheremesh_required_bytes_t;

spheremesh_required_bytes_t spheremesh_calculate_required_bytes(int subdivisions)
{
	int in_vertices_capacity = 6;
	int in_triangles_count = 8;
	int out_vertices_capacity = 0;
	int out_triangles_count = 0;
	int edge_vertex_pairs_capacity = 0;

	for (int s = 0; s < subdivisions; ++s)
	{
		out_vertices_capacity = in_vertices_capacity + 3 * in_triangles_count;
		out_triangles_count = 4 * in_triangles_count;

		edge_vertex_pairs_capacity = 3 * in_triangles_count;

		in_vertices_capacity = out_vertices_capacity;
		in_triangles_count = out_triangles_count;
	}

	int in_indices_count = 3 * in_triangles_count;
	int out_indices_count = 3 * out_triangles_count;

	spheremesh_required_bytes_t bytes;
	bytes.in_vertices_bytes = in_vertices_capacity * sizeof(spheremesh_vec3_t);
	bytes.in_indices_bytes = in_indices_count * sizeof(int);
	bytes.out_vertices_bytes = out_vertices_capacity * sizeof(spheremesh_vec3_t);
	bytes.out_indices_bytes = out_indices_count * sizeof(int);
	bytes.edge_vertex_pairs_bytes = edge_vertex_pairs_capacity * sizeof(spheremesh_edge_vertex_pair_t);
	return bytes;
}

int spheremesh_required_bytes(int subdivisions)
{
	spheremesh_required_bytes_t bytes = spheremesh_calculate_required_bytes(subdivisions);

	int total_bytes = 0;
	total_bytes += bytes.in_vertices_bytes;
	total_bytes += bytes.in_indices_bytes;
	total_bytes += bytes.out_vertices_bytes;
	total_bytes += bytes.out_indices_bytes;
	total_bytes += bytes.edge_vertex_pairs_bytes;
	
	return total_bytes;
}

spheremesh_output_t spheremesh_generate_mesh(void* required_memory, int subdivisions)
{
	spheremesh_required_bytes_t bytes = spheremesh_calculate_required_bytes(subdivisions);

	spheremesh_vec3_t* in_vertices = (spheremesh_vec3_t*)required_memory;
	int* in_indices = (int*)((char*)(in_vertices) + bytes.in_vertices_bytes);
	spheremesh_vec3_t* out_vertices = (spheremesh_vec3_t*)((char*)(in_indices) +bytes.in_indices_bytes);
	int* out_indices = (int*)((char*)(out_vertices) +bytes.out_vertices_bytes);
	spheremesh_edge_vertex_pair_t* pairs = (spheremesh_edge_vertex_pair_t*)((char*)(out_indices) +bytes.out_indices_bytes);

	spheremesh_mesh_t in_mesh;
	in_mesh.vertex_count = 0;
	in_mesh.vertices = in_vertices;
	in_mesh.index_count = 0;
	in_mesh.indices = in_indices;

	spheremesh_mesh_set_octahedron(&in_mesh);

	for (int s = 0; s < subdivisions; ++s)
	{
		spheremesh_mesh_t out_mesh;
		out_mesh.vertex_count = 0;
		out_mesh.vertices = out_vertices;
		out_mesh.index_count = 0;
		out_mesh.indices = out_indices;

		spheremesh_edge_vertex_map_t map;
		map.pair_count = 0;
		map.pairs = pairs;

		spheremesh_subdivide_mesh(&out_mesh, &in_mesh, &map);

		// in = out
		in_mesh.vertex_count = out_mesh.vertex_count;
		for (int i = 0; i < out_mesh.vertex_count; ++i)
		{
			in_mesh.vertices[i] = out_mesh.vertices[i];
		}
		
		in_mesh.index_count = out_mesh.index_count;
		for (int i = 0; i < out_mesh.index_count; ++i)
		{
			in_mesh.indices[i] = out_mesh.indices[i];
		}
	}

	spheremesh_output_t output;

	output.vertex_count = in_mesh.vertex_count;
	output.vertices = (float*)malloc(in_mesh.vertex_count * 3 * sizeof(float));
	for (int i = 0; i < in_mesh.vertex_count; ++i)
	{
		spheremesh_vec3_t v = in_mesh.vertices[i];
		
		output.vertices[3 * i + 0] = v.x;
		output.vertices[3 * i + 1] = v.y;
		output.vertices[3 * i + 2] = v.z;
	}

	output.index_count = in_mesh.index_count;
	output.indices = (int*)malloc(in_mesh.index_count * sizeof(int));
	for (int i = 0; i < in_mesh.index_count; ++i)
	{
		output.indices[i] = in_mesh.indices[i];
	}

	return output;
}

#endif // #if !defined(spheremesh_implementation_once)

#endif // #if defined(spheremesh_implementation)
