/*
* The MIT License
* 
* Copyright 2020 Irlan Robson
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy 
* of this software and associated documentation files (the "Software"), to deal 
* in the Software without restriction, including without limitation the rights 
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
* copies of the Software, and to permit persons to whom the Software is furnished 
* to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all 
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
* PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
* HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
* CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
* OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef spherecamera_h

#include <cmath>

#define SC_PI float(3.14159265358979323846)

#ifndef SC_ASSERT
	#include <assert.h>
	#define SC_ASSERT(condition) assert(condition)
#endif

struct sc_vec2
{
	sc_vec2() { }
	sc_vec2(float _x, float _y) : x(_x), y(_y) { }
	
	float x, y;
};

struct sc_vec3
{
	sc_vec3() { } 
	sc_vec3(float _x, float _y, float _z) : x(_x), y(_y), z(_z) { } 
	
	void operator+=(sc_vec3 v) { x += v.x; y += v.y; z += v.z; }
	void operator-=(sc_vec3 v) { x -= v.x; y -= v.y; z -= v.z; }
	
	float x, y, z;
};

inline sc_vec3 operator+(sc_vec3 a, sc_vec3 b)
{
	return sc_vec3(a.x + b.x, a.y + b.y, a.z + b.z);
}

inline sc_vec3 operator-(sc_vec3 a, sc_vec3 b)
{
	return sc_vec3(a.x - b.x, a.y - b.y, a.z - b.z);
}

inline sc_vec3 operator*(float s, sc_vec3 v)
{
	return sc_vec3(s * v.x, s * v.y, s * v.z);
}

inline sc_vec3 operator-(sc_vec3 v)
{
	return sc_vec3(-v.x, -v.y, -v.z);
}

inline sc_vec3 sc_normalize(sc_vec3 v)
{
	float len = sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
	if (len > 0.0f)
	{
		return (1.0f / len) * v; 	
	}
	return v;
}

struct sc_mat33
{
	sc_mat33() { }
	sc_mat33(sc_vec3 _x, sc_vec3 _y, sc_vec3 _z) : x(_x), y(_y), z(_z) { } 
	
	sc_vec3 x, y, z;
};

inline sc_vec3 operator*(sc_mat33 A, sc_vec3 v)
{
	return v.x * A.x + v.y * A.y + v.z * A.z;
}

inline sc_mat33 sc_transpose(sc_mat33 A)
{
	return sc_mat33(
		sc_vec3(A.x.x, A.y.x, A.z.x), 
		sc_vec3(A.x.y, A.y.y, A.z.y),
		sc_vec3(A.x.z, A.y.z, A.z.z));
}

struct sc_vec4
{
	sc_vec4() { } 
	sc_vec4(float _x, float _y, float _z, float _w) : x(_x), y(_y), z(_z), w(_w) { } 
	
	float x, y, z, w;
};

inline sc_vec4 operator+(sc_vec4 a, sc_vec4 b)
{
	return sc_vec4(a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w);
}

inline sc_vec4 operator*(float s, sc_vec4 v)
{
	return sc_vec4(s * v.x, s * v.y, s * v.z, s * v.w);
}

struct sc_mat44
{
	sc_mat44() { }
	sc_mat44(sc_vec4 _x, sc_vec4 _y, sc_vec4 _z, sc_vec4 _w) : x(_x), y(_y), z(_z), w(_w) { } 
	
	sc_vec4 x, y, z, w;
};

inline sc_vec4 operator*(sc_mat44 A, sc_vec4 v)
{
	return v.x * A.x + v.y * A.y + v.z * A.z + v.w * A.w;
}

inline sc_mat44 operator*(sc_mat44 A, sc_mat44 B)
{
	return sc_mat44(A * B.x, A * B.y, A * B.z, A * B.w);
}

// An spherical camera controller (also called orbit camera).
// This accepts spherical and Cartesian coordinates as input.
// See https://en.wikipedia.org/wiki/Spherical_coordinate_system
class sc_camera
{
public:
	// Default ctor.
	sc_camera();
	
	// Set the camera width.
	void set_width(float width) { m_width = width; }
	
	// Get the camera width.
	float get_width() const { return m_width; } 
	
	// Set the camera height.
	void set_height(float height) { m_height = height; }
	
	// Get the camera height.
	float get_height() const { return m_height; }
	
	// Set the near plane distance.
	void set_z_near(float z_near) { m_z_near = z_near; }
	
	// Get the near plane distance.
	float get_z_near() const { return m_z_near; }
	
	// Set the far plane distance.
	void set_z_far(float z_far) { m_z_far = z_far; }
	
	// Get the far plane distance.
	float get_z_far() const { return m_z_far; }
	
	// Set the full field of view angle.
	void set_y_fov(float y_fov) { m_y_fov = y_fov; };
	
	// Get the full field of view angle.
	float get_y_fov() const { return m_y_fov; }
	
	// Set the radius coordinate.
	void set_radius(float radius) { m_r = radius; }
	
	// Get the radius coordinate.
	float get_radius() const { return m_r; }
	
	// Set the polar angle in the range [0, pi].
	void set_polar_angle(float angle);
	
	// Get the polar angle in the range [0, pi].
	float get_polar_angle() const { return m_theta; }
	
	// Set the azimuthal angle in the range [0, 2*pi].
	void set_azimuthal_angle(float angle);
	
	// Get the azimuthal angle in the range [0, 2*pi].
	float get_azimuthal_angle() const { return m_phi; }
	
	// Set the sphere center.
	void set_center(sc_vec3 center) { m_center = center; }
	
	// Get the sphere center.
	sc_vec3 get_center() const { return m_center; }
	
	// Translate the sphere center in the direction of the camera x axis.
	void translate_x_axis(float distance);
	
	// Translate the sphere center in the direction of the camera y axis.
	void translate_y_axis(float distance);
	
	// Translate the sphere center in the direction of the camera z axis.
	void translate_z_axis(float distance);
	
	// Increase/decrease the radius.
	void zoom(float distance);
	
	// Increase/decrease the polar angle.
	void rotate_polar(float angle);
	
	// Increase/decrease the azimuthal angle.
	void rotate_azimuthal(float angle);	
	
	// Look at a given target position from a given eye position.
	void look_at(sc_vec3 eye_position, sc_vec3 target_position);
	
	// Set the camera position from three Cartesian coordinates.
	void set_position(sc_vec3 pw);
	
	// Get the camera position in Cartesian coordinates.
	sc_vec3 build_position() const;
	
	// Get the camera x axis.
	sc_vec3 build_x_axis() const;
	
	// Get the camera y axis.
	sc_vec3 build_y_axis() const;
	
	// Get the camera z axis.
	sc_vec3 build_z_axis() const;
	
	// Get the camera rotation matrix.
	sc_mat33 build_rotation() const;
	
	// Get the camera view matrix.
	sc_mat44 build_view_matrix() const;
	
	// Get the camera projection matrix.
	sc_mat44 build_projection_matrix() const;
	
	// Convert a point in world space to screen space.
	sc_vec2 convert_world_to_screen(sc_vec3 pw) const;
	
	// Convert a point in screen space to world space.
	sc_vec3 convert_screen_to_world(sc_vec2 ps) const;
private:
	float m_width, m_height;
	float m_z_near;
	float m_z_far;
	float m_y_fov;
	
	float m_r;
	float m_theta;
	float m_phi;
	sc_vec3 m_center;
};

inline sc_camera::sc_camera()
{
	m_width = 1024.0f;
	m_height = 768.0f;
	m_z_near = 1.0f;
	m_z_far = 1000.0f;
	m_y_fov = 0.25f * SC_PI;
	m_r = 1.0f;
	m_theta = 0.0f;
	m_phi = 0.0f;
	m_center = sc_vec3(0.0f, 0.0f, 0.0f);
}

inline void sc_camera::set_polar_angle(float angle)
{
	SC_ASSERT(angle >= 0.0f);
	SC_ASSERT(angle <= SC_PI);
	m_theta = angle;
}
	
inline void sc_camera::set_azimuthal_angle(float angle)
{
	SC_ASSERT(angle >= 0.0f);
	SC_ASSERT(angle <= 2.0f * SC_PI);
	m_phi = angle;
}

inline void sc_camera::translate_x_axis(float distance)
{
	sc_vec3 x = build_x_axis();
	m_center += distance * x;
}
	
inline void sc_camera::translate_y_axis(float distance)
{
	sc_vec3 y = build_y_axis();
	m_center += distance * y;
}

inline void sc_camera::translate_z_axis(float distance)
{
	sc_vec3 z = build_z_axis();
	m_center += distance * z;
}
	
inline void sc_camera::zoom(float distance)
{
	m_r += distance;
}

inline void sc_camera::look_at(sc_vec3 eye_position, sc_vec3 target_position)
{
	set_position(eye_position);
	m_center = target_position;
}
	
inline void sc_camera::rotate_polar(float angle)
{
	if (m_theta + angle < 0.0f)
	{
		m_theta = 0.0f;
	}
	else if (m_theta + angle > SC_PI)
	{
		m_theta = SC_PI;
	}
	else
	{
		m_theta += angle;
	}
}

inline void sc_camera::rotate_azimuthal(float angle)
{
	m_phi += angle;
	
	// Normalize angle in range [0, 2*pi]
	float two_pi = 2.0f * SC_PI;
	m_phi -= floor(m_phi / two_pi) * two_pi;
}

// This detects the quadrant the given point is on  
// and returns the corresponding angle in the range [0, 2*pi].
static float sc_azimuthal_angle(float x, float y)
{
	if (x > 0.0f && y > 0.0f)
	{
		return atan(y / x);
	}
	
	if (x < 0.0f && y > 0.0f)
	{
		return 0.5f * SC_PI + atan(-x / y);
	}
	
	if (x < 0.0f && y < 0.0f)
	{
		return SC_PI + atan(y / x);	
	}
	
	if (x > 0.0f && y < 0.0f)
	{
		return SC_PI + 0.5f * SC_PI + atan(-x / y);
	}
	
	if (x == 0.0f && y > 0.0f)
	{
		return 0.5f * SC_PI;
	}
	
	if (x == 0.0f && y < 0.0f)
	{
		return SC_PI + 0.5f * SC_PI;
	}
	
	if (x > 0.0f && y == 0.0f)
	{
		return 0.0f;
	}
	
	if (x < 0.0f && y == 0.0f)
	{
		return SC_PI;
	}
	
	if (x == 0.0f && y == 0.0f)
	{
		return 0.0f;	
	}
	
	SC_ASSERT(false);
}

// This detects the quadrant the given point is on  
// and returns the corresponding angle in the range [0, pi].
static float sc_polar_angle(float x, float y)
{
	if (x > 0.0f && y > 0.0f)
	{
		return atan(y / x);
	}
	
	if (x < 0.0f && y > 0.0f)
	{
		return 0.5f * SC_PI + atan(-x / y);
	}
	
	if (x < 0.0f && y < 0.0f)
	{
		return atan(x / y);
	}
	
	if (x > 0.0f && y < 0.0f)
	{
		return atan(-y / x);
	}
	
	if (x == 0.0f && y > 0.0f)
	{
		return 0.5f * SC_PI;
	}
	
	if (x == 0.0f && y < 0.0f)
	{
		return 0.5f * SC_PI;
	}
	
	if (y == 0.0f && x > 0.0f)
	{
		return 0.0f;
	}
	
	if (y == 0.0f && x < 0.0f)
	{
		return SC_PI;
	}
	
	if (x == 0.0f && y == 0.0f)
	{
		return 0;
	}
		
	SC_ASSERT(false);
}

inline void sc_camera::set_position(sc_vec3 pw)
{
	float x = pw.x, y = pw.y, z = pw.z;
	
	float r = sqrt(x * x + y * y + z * z);
	
	float phi = sc_azimuthal_angle(z, x);
	SC_ASSERT(phi >= 0.0f && phi <= 2.0f * SC_PI);
	
	float s = sqrt(z * z + x * x);
	float theta = sc_polar_angle(y, s);
	SC_ASSERT(theta >= 0.0f && theta <= SC_PI);
	
	m_r = r;
	m_phi = phi;
	m_theta = theta;
}
	
inline sc_vec3 sc_camera::build_position() const
{
	sc_vec3 z = build_z_axis();
	return m_center + m_r * z;
}

inline sc_vec3 sc_camera::build_x_axis() const
{
	sc_vec3 x;
	x.x = cos(m_phi);
	x.y = 0.0f;
	x.z = -sin(m_phi);
	return x;
}

inline sc_vec3 sc_camera::build_y_axis() const
{
	sc_vec3 y;
	y.x = -cos(m_theta) * sin(m_phi);
	y.y = sin(m_theta);
	y.z = -cos(m_theta) * cos(m_phi);
	return y;
}

inline sc_vec3 sc_camera::build_z_axis() const
{
	sc_vec3 z;
	z.x = sin(m_theta) * sin(m_phi);
	z.y = cos(m_theta);
	z.z = sin(m_theta) * cos(m_phi);
	return z;
}

inline sc_mat33 sc_camera::build_rotation() const
{
	sc_vec3 x = build_x_axis();
	sc_vec3 y = build_y_axis();
	sc_vec3 z = build_z_axis();
	
	return sc_mat33(x, y, z);
}

inline sc_mat44 sc_camera::build_view_matrix() const
{
	sc_vec3 translation = build_position();
	sc_mat33 rotation = build_rotation();
	
	sc_mat33 R = sc_transpose(rotation);
	sc_vec3 t = -(R * translation);
	
	return sc_mat44(
		sc_vec4(R.x.x, R.x.y, R.x.z, 0.0f),
		sc_vec4(R.y.x, R.y.y, R.y.z, 0.0f),
		sc_vec4(R.z.x, R.z.y, R.z.z, 0.0f),
		sc_vec4(t.x, t.y, t.z, 1.0f));
}

inline sc_mat44 sc_camera::build_projection_matrix() const
{
	float w = m_width, h = m_height;
	float zn = m_z_near, zf = m_z_far;
	float yfov = m_y_fov;
	float ratio = w / h;
	
	float t = tan(0.5f * yfov);
	float sx = 1.0f / (ratio * t);
	float sy = 1.0f / t;
	
	float sz = (zn + zf) / (zn - zf);
	float tz = (zf * zn) / (zn - zf);

	sc_mat44 m;
	m.x = sc_vec4(sx, 0.0f, 0.0f, 0.0f);
	m.y = sc_vec4(0.0f, sy, 0.0f, 0.0f);
	m.z = sc_vec4(0.0f, 0.0f, sz, -1.0f);
	m.w = sc_vec4(0.0f, 0.0f, tz, 0.0f);
	return m;
}

inline sc_vec2 sc_camera::convert_world_to_screen(sc_vec3 pw3) const
{
	float w = m_width, h = m_height;

	sc_mat44 P = build_projection_matrix();
	sc_mat44 V = build_view_matrix();

	sc_vec4 pw(pw3.x, pw3.y, pw3.z, 1.0f);
	
	sc_vec4 pp = P * V * pw;
	
	sc_vec3 pn(pp.x, pp.y, pp.z);
	float inv_w = pp.w != 0.0f ? 1.0f / pp.w : 1.0f;
	pn = inv_w * pn;
	
	float u = 0.5f * (pn.x + 1.0f);
	float v = 0.5f * (pn.y + 1.0f);

	sc_vec2 ps;
	ps.x = u * w;
	ps.y = (1.0f - v) * h;
	return ps;
}

inline sc_vec3 sc_camera::convert_screen_to_world(sc_vec2 ps) const
{
	float w = m_width, h = m_height;

	float t = tan(0.5f * m_y_fov);
	float ratio = w / h;

	sc_vec3 vv;
	vv.x = 2.0f * ratio * ps.x / w - ratio;
	vv.y = -2.0f * ps.y / h + 1.0f;
	vv.z = -1.0f / t;

	sc_mat33 R = build_rotation();

	sc_vec3 vw = R * vv;
	
	return sc_normalize(vw);
}

#define spherecamera_h

#endif // #ifndef spherecamera_h
