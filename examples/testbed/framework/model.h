/*
* Copyright (c) 2016-2019 Irlan Robson 
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

#ifndef MODEL_H
#define MODEL_H

#include "gl_debugdraw.h"
#include "profiler.h"
#include "spherecamera.h"
#include <bounce/common/graphics/debugdraw.h>

extern sc_camera* g_camera;
extern b3DebugPrimitives* g_primitives;
extern profiler_tree_t* g_profiler;

class Test;
class ViewModel;

class Model
{
public:
	Model();
	~Model();

	void Action_SetTest();
	void Action_PlayPause();
	void Action_SinglePlay();
	void Action_ResetCamera();

	void Command_Press_Key(int button);
	void Command_Release_Key(int button);
	void Command_Press_Mouse_Left(const b3Vec2& ps);
	void Command_Release_Mouse_Left(const b3Vec2& ps);
	void Command_Move_Cursor(const b3Vec2& ps);

	void Command_ResizeCamera(scalar w, scalar h);
	void Command_RotateCameraX(scalar angle);
	void Command_RotateCameraY(scalar angle);
	void Command_TranslateCameraX(scalar d);
	void Command_TranslateCameraY(scalar d);
	void Command_ZoomCamera(scalar d);

	void Update();

	bool IsPaused() const { return m_pause; }
private:
	friend class ViewModel;

	ViewModel* m_viewModel;
	sc_camera m_camera;
	b3DebugPrimitives m_debugPrimitives;
	GLDebugDraw m_debugDrawCallback;
	profiler_tree_t* m_profiler;
	Test* m_test;
	bool m_setTest;
	bool m_pause;
	bool m_singlePlay;
};

inline void Model::Action_SetTest()
{
	m_setTest = true;
}

inline void Model::Action_PlayPause()
{
	m_pause = !m_pause;
}

inline void Model::Action_SinglePlay()
{
	m_pause = true;
	m_singlePlay = true;
}

inline void Model::Action_ResetCamera()
{
	sc_vec3 position(20.0f, 20.0f, 40.0f);
	m_camera.set_position(position);
}

inline void Model::Command_ResizeCamera(scalar w, scalar h)
{
	m_camera.set_width(w);
	m_camera.set_height(h);
}

inline void Model::Command_RotateCameraX(scalar angle)
{
	m_camera.rotate_polar(angle);
}

inline void Model::Command_RotateCameraY(scalar angle)
{
	m_camera.rotate_azimuthal(angle);
}

inline void Model::Command_TranslateCameraX(scalar d)
{
	m_camera.translate_x_axis(d);
}

inline void Model::Command_TranslateCameraY(scalar d)
{
	m_camera.translate_y_axis(d);
}

inline void Model::Command_ZoomCamera(scalar d)
{
	m_camera.zoom(d);
}

#endif
