/*
* Copyright (c) 2016-2019 Irlan Robson 
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

#if !defined(gl_primitive_h)

#include <stdint.h>

typedef struct gl_primitive_points_t gl_primitive_points_t;

gl_primitive_points_t* gl_primitive_points_create(uint32_t point_capacity);
void gl_primitive_points_destroy(gl_primitive_points_t* points);
void gl_primitive_points_vertex(gl_primitive_points_t* points, float x, float y, float z, float r, float g, float b, float a, float point_size);
void gl_primitive_points_flush(gl_primitive_points_t* points, float* mvp);

typedef struct gl_primitive_lines_t gl_primitive_lines_t;

gl_primitive_lines_t* gl_primitive_lines_create(uint32_t line_capacity);
void gl_primitive_lines_destroy(gl_primitive_lines_t* lines);
void gl_primitive_lines_vertex(gl_primitive_lines_t* lines, float x, float y, float z, float r, float g, float b, float a);
void gl_primitive_lines_flush(gl_primitive_lines_t* lines, float* mvp);

typedef struct gl_primitive_triangles_t gl_primitive_triangles_t;

gl_primitive_triangles_t* gl_primitive_triangles_create(uint32_t triangle_capacity);
void gl_primitive_triangles_destroy(gl_primitive_triangles_t* triangles);
void gl_primitive_triangles_vertex(gl_primitive_triangles_t* triangles, float x, float y, float z, float r, float g, float b, float a, float nx, float ny, float nz);
void gl_primitive_triangles_flush(gl_primitive_triangles_t* triangles, float* mvp);

#define gl_primitive_h // #if !defined(gl_primitive_h)

#endif // #if !defined(gl_primitive_h)

#if defined(gl_primitive_implementation)

#if !defined (gl_primitive_implementation_once)
#define gl_primitive_implementation_once

#include <stdio.h>
#include <assert.h>

#if !defined(gl_primitive_alloc_and_free)

#include <stdlib.h> // malloc, free, NULL

#define gl_primitive_alloc(size) malloc(size)
#define gl_primitive_free(mem) free(mem)
	
#define gl_primitive_alloc_and_free // #if !defined(gl_primitive_alloc_and_free)

#endif // #if !defined(gl_primitive_alloc_and_free)

#define gl_primitive_ptr_add(ptr, size) ((void*)(((char*)ptr) + (size)))

void gl_primitive_assert_gl()
{
	GLenum error_code = glGetError();
	if (error_code != GL_NO_ERROR)
	{
		printf("OpenGL error code = %d\n", error_code);
		assert(false);
	}
}

void gl_primitive_print_log(GLuint id)
{
	GLint log_length = 0;
	if (glIsShader(id))
	{
		glGetShaderiv(id, GL_INFO_LOG_LENGTH, &log_length);
	}
	else if (glIsProgram(id))
	{
		glGetProgramiv(id, GL_INFO_LOG_LENGTH, &log_length);
	}
	else
	{
		printf("Not a shader or a program\n");
		return;
	}

	char* log = (char*)malloc(log_length);

	if (glIsShader(id))
	{
		glGetShaderInfoLog(id, log_length, NULL, log);
	}
	else if (glIsProgram(id))
	{
		glGetProgramInfoLog(id, log_length, NULL, log);
	}

	printf("%s", log);
	free(log);
}

GLuint gl_primitive_create_shader(const char* source, GLenum type)
{
	GLuint shader = glCreateShader(type);

	const char* sources[] = { source };
	glShaderSource(shader, 1, sources, NULL);
	glCompileShader(shader);

	GLint status = GL_FALSE;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE)
	{
		printf("Error compiling %d shader.\n", type);
		gl_primitive_print_log(shader);
		glDeleteShader(shader);
		assert(false);
		return 0;
	}

	return shader;
}

GLuint gl_primitive_create_shader_program(const char* vs, const char* fs)
{
	GLuint vs_id = gl_primitive_create_shader(vs, GL_VERTEX_SHADER);
	GLuint fs_id = gl_primitive_create_shader(fs, GL_FRAGMENT_SHADER);
	assert(vs_id != 0 && fs_id != 0);

	GLuint program = glCreateProgram();
	glAttachShader(program, vs_id);
	glAttachShader(program, fs_id);
	glLinkProgram(program);

	glDeleteShader(vs_id);
	glDeleteShader(fs_id);

	GLint status = GL_FALSE;
	glGetProgramiv(program, GL_LINK_STATUS, &status);
	assert(status != GL_FALSE);

	return program;
}

struct gl_primitive_points_t
{
	uint32_t vertex_capacity;
	float* positions;
	float* colors;
	float* sizes;
	uint32_t vertex_count;

	GLuint vbos[3];

	GLuint program;
	GLuint position_attribute;
	GLuint color_attribute;
	GLuint size_attribute;
	GLuint projection_uniform;
};

gl_primitive_points_t* gl_primitive_points_create(uint32_t point_capacity)
{
	gl_primitive_points_t* points = (gl_primitive_points_t*)gl_primitive_alloc(sizeof(gl_primitive_points_t));

	const char* vs = \
		"#version 120\n"
		"uniform mat4 m_projection;\n"
		"attribute vec3 v_position;\n"
		"attribute vec4 v_color;\n"
		"attribute float v_size;\n"
		"varying vec4 f_color;\n"
		"void main()\n"
		"{\n"
		"	f_color = v_color;\n"
		"	gl_Position = m_projection * vec4(v_position, 1.0f);\n"
		"   gl_PointSize = v_size;\n"
		"}\n";

	const char* fs = \
		"#version 120\n"
		"varying vec4 f_color;\n"
		"void main(void)\n"
		"{\n"
		"	gl_FragColor = f_color;\n"
		"}\n";

	points->program = gl_primitive_create_shader_program(vs, fs);
	points->position_attribute = glGetAttribLocation(points->program, "v_position");
	points->color_attribute = glGetAttribLocation(points->program, "v_color");
	points->size_attribute = glGetAttribLocation(points->program, "v_size");
	points->projection_uniform = glGetUniformLocation(points->program, "m_projection");

	points->vertex_capacity = point_capacity;
	points->positions = (float*)gl_primitive_alloc(point_capacity * 3 * sizeof(float));
	points->colors = (float*)gl_primitive_alloc(point_capacity * 4 * sizeof(float));
	points->sizes = (float*)gl_primitive_alloc(point_capacity * sizeof(float));
	points->vertex_count = 0;

	glGenBuffers(3, points->vbos);

	glBindBuffer(GL_ARRAY_BUFFER, points->vbos[0]);
	glBufferData(GL_ARRAY_BUFFER, point_capacity * 3 * sizeof(float), points->positions, GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, points->vbos[1]);
	glBufferData(GL_ARRAY_BUFFER, point_capacity * 4 * sizeof(float), points->colors, GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, points->vbos[2]);
	glBufferData(GL_ARRAY_BUFFER, point_capacity * sizeof(float), points->sizes, GL_DYNAMIC_DRAW);

	gl_primitive_assert_gl();

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	return points;
}

void gl_primitive_points_destroy(gl_primitive_points_t* points)
{
	glDeleteProgram(points->program);
	glDeleteBuffers(3, points->vbos);

	gl_primitive_free(points->positions);
	gl_primitive_free(points->colors);
	gl_primitive_free(points->sizes);

	gl_primitive_free(points);
}

void gl_primitive_points_vertex(gl_primitive_points_t* points, float x, float y, float z, float r, float g, float b, float a, float point_size)
{
	assert(points->vertex_count < points->vertex_capacity);
	points->positions[3 * points->vertex_count] = x;
	points->positions[3 * points->vertex_count + 1] = y;
	points->positions[3 * points->vertex_count + 2] = z;
	points->colors[4 * points->vertex_count] = r;
	points->colors[4 * points->vertex_count + 1] = g;
	points->colors[4 * points->vertex_count + 2] = b;
	points->colors[4 * points->vertex_count + 3] = a;
	points->sizes[points->vertex_count] = point_size;
	++points->vertex_count;
}

void gl_primitive_points_flush(gl_primitive_points_t* points, float* mvp)
{
	if (points->vertex_count == 0)
	{
		return;
	}

	glUseProgram(points->program);

	glUniformMatrix4fv(points->projection_uniform, 1, GL_FALSE, mvp);

	glBindBuffer(GL_ARRAY_BUFFER, points->vbos[0]);
	glBufferSubData(GL_ARRAY_BUFFER, 0, points->vertex_count * 3 * sizeof(float), points->positions);
	glVertexAttribPointer(points->position_attribute, 3, GL_FLOAT, GL_FALSE, 0, gl_primitive_ptr_add(NULL, 0));
	glEnableVertexAttribArray(points->position_attribute);

	glBindBuffer(GL_ARRAY_BUFFER, points->vbos[1]);
	glBufferSubData(GL_ARRAY_BUFFER, 0, points->vertex_count * 4 * sizeof(float), points->colors);
	glEnableVertexAttribArray(points->color_attribute);
	glVertexAttribPointer(points->color_attribute, 4, GL_FLOAT, GL_FALSE, 0, gl_primitive_ptr_add(NULL, 0));

	glBindBuffer(GL_ARRAY_BUFFER, points->vbos[2]);
	glBufferSubData(GL_ARRAY_BUFFER, 0, points->vertex_count * sizeof(float), points->sizes);
	glEnableVertexAttribArray(points->size_attribute);
	glVertexAttribPointer(points->size_attribute, 1, GL_FLOAT, GL_FALSE, 0, gl_primitive_ptr_add(NULL, 0));

	glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
	glDrawArrays(GL_POINTS, 0, points->vertex_count);
	glDisable(GL_VERTEX_PROGRAM_POINT_SIZE);
	
	points->vertex_count = 0;

	glDisableVertexAttribArray(points->size_attribute);
	glDisableVertexAttribArray(points->color_attribute);
	glDisableVertexAttribArray(points->position_attribute);

	gl_primitive_assert_gl();

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glUseProgram(0);
}

struct gl_primitive_lines_t
{
	uint32_t vertex_capacity;
	float* positions;
	float* colors;
	uint32_t vertex_count;

	GLuint vbos[2];

	GLuint program;
	GLuint position_attribute;
	GLuint color_attribute;
	GLuint projection_uniform;
};

gl_primitive_lines_t* gl_primitive_lines_create(uint32_t line_capacity)
{
	gl_primitive_lines_t* lines = (gl_primitive_lines_t*)gl_primitive_alloc(sizeof(gl_primitive_lines_t));

	const char* vs = \
		"#version 120\n"
		"uniform mat4 m_projection;\n"
		"attribute vec3 v_position;\n"
		"attribute vec4 v_color;\n"
		"varying vec4 f_color;\n"
		"void main(void)\n"
		"{\n"
		"	f_color = v_color;\n"
		"	gl_Position = m_projection * vec4(v_position, 1.0f);\n"
		"}\n";

	const char* fs = \
		"#version 120\n"
		"varying vec4 f_color;\n"
		"void main(void)\n"
		"{\n"
		"	gl_FragColor = f_color;\n"
		"}\n";
	
	lines->program = gl_primitive_create_shader_program(vs, fs);
	lines->position_attribute = glGetAttribLocation(lines->program, "v_position");
	lines->color_attribute = glGetAttribLocation(lines->program, "v_color");
	lines->projection_uniform = glGetUniformLocation(lines->program, "m_projection");

	lines->vertex_capacity = 2 * line_capacity;
	lines->positions = (float*)gl_primitive_alloc(lines->vertex_capacity * 3 * sizeof(float));
	lines->colors = (float*)gl_primitive_alloc(lines->vertex_capacity * 4 * sizeof(float));
	lines->vertex_count = 0;

	glGenBuffers(2, lines->vbos);

	glBindBuffer(GL_ARRAY_BUFFER, lines->vbos[0]);
	glBufferData(GL_ARRAY_BUFFER, lines->vertex_capacity * 3 * sizeof(float), lines->positions, GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, lines->vbos[1]);
	glBufferData(GL_ARRAY_BUFFER, lines->vertex_capacity * 4 * sizeof(float), lines->colors, GL_DYNAMIC_DRAW);

	gl_primitive_assert_gl();

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	return lines;
}

void gl_primitive_lines_destroy(gl_primitive_lines_t* lines)
{
	glDeleteProgram(lines->program);
	glDeleteBuffers(2, lines->vbos);

	gl_primitive_free(lines->positions);
	gl_primitive_free(lines->colors);

	gl_primitive_free(lines);
}

void gl_primitive_lines_vertex(gl_primitive_lines_t* lines, float x, float y, float z, float r, float g, float b, float a)
{
	assert(lines->vertex_count < lines->vertex_capacity);
	lines->positions[3 * lines->vertex_count] = x;
	lines->positions[3 * lines->vertex_count + 1] = y;
	lines->positions[3 * lines->vertex_count + 2] = z;
	lines->colors[4 * lines->vertex_count] = r;
	lines->colors[4 * lines->vertex_count + 1] = g;
	lines->colors[4 * lines->vertex_count + 2] = b;
	lines->colors[4 * lines->vertex_count + 3] = a;
	++lines->vertex_count;
}

void gl_primitive_lines_flush(gl_primitive_lines_t* lines, float* mvp)
{
	if (lines->vertex_count == 0)
	{
		return;
	}

	glUseProgram(lines->program);

	glUniformMatrix4fv(lines->projection_uniform, 1, GL_FALSE, mvp);

	glBindBuffer(GL_ARRAY_BUFFER, lines->vbos[0]);
	glBufferSubData(GL_ARRAY_BUFFER, 0, lines->vertex_count * 3 * sizeof(float), lines->positions);
	glVertexAttribPointer(lines->position_attribute, 3, GL_FLOAT, GL_FALSE, 0, gl_primitive_ptr_add(NULL, 0));
	glEnableVertexAttribArray(lines->position_attribute);

	glBindBuffer(GL_ARRAY_BUFFER, lines->vbos[1]);
	glBufferSubData(GL_ARRAY_BUFFER, 0, lines->vertex_count * 4 * sizeof(float), lines->colors);
	glVertexAttribPointer(lines->color_attribute, 4, GL_FLOAT, GL_FALSE, 0, gl_primitive_ptr_add(NULL, 0));
	glEnableVertexAttribArray(lines->color_attribute);

	glDrawArrays(GL_LINES, 0, lines->vertex_count);

	lines->vertex_count = 0;

	gl_primitive_assert_gl();

	glDisableVertexAttribArray(lines->color_attribute);
	glDisableVertexAttribArray(lines->position_attribute);
	
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glUseProgram(0);
}

struct gl_primitive_triangles_t
{
	uint32_t vertex_capacity;
	float* positions;
	float* colors;
	float* normals;
	uint32_t vertex_count;

	GLuint vbos[3];

	GLuint program;
	GLuint position_attribute;
	GLuint color_attribute;
	GLuint normal_attribute;
	GLuint projection_uniform;
};

gl_primitive_triangles_t* gl_primitive_triangles_create(uint32_t triangle_capacity)
{
	gl_primitive_triangles_t* triangles = (gl_primitive_triangles_t*)gl_primitive_alloc(sizeof(gl_primitive_triangles_t));

	const char* vs = \
		"#version 120\n"
		"uniform mat4 m_projection;\n"
		"attribute vec3 v_position;\n"
		"attribute vec4 v_color;\n"
		"attribute vec3 v_normal;\n"
		"varying vec4 f_color;\n"
		"void main(void)\n"
		"{\n"
		"	vec3 La = vec3(0.5f, 0.5f, 0.5f);\n"
		"	vec3 Ld = vec3(0.5f, 0.5f, 0.5f);\n"
		"	vec3 L = vec3(0.0f, 0.3f, 0.7f);\n"
		"	vec3 Ma = v_color.xyz;\n"
		"	vec3 Md = v_color.xyz;\n"
		"	vec3 a = La * Ma;\n"
		"	vec3 d = max(dot(v_normal, L), 0.0f) * Ld * Md;\n"
		"	f_color = vec4(a + d, v_color.w);\n"
		"	gl_Position = m_projection * vec4(v_position, 1.0f);\n"
		"}\n";

	const char* fs = \
		"#version 120\n"
		"varying vec4 f_color;\n"
		"void main(void)\n"
		"{\n"
		"	gl_FragColor = f_color;\n"
		"}\n";
	
	triangles->program = gl_primitive_create_shader_program(vs, fs);
	triangles->position_attribute = glGetAttribLocation(triangles->program, "v_position");
	triangles->color_attribute = glGetAttribLocation(triangles->program, "v_color");
	triangles->normal_attribute = glGetAttribLocation(triangles->program, "v_normal");
	triangles->projection_uniform = glGetUniformLocation(triangles->program, "m_projection");

	triangles->vertex_capacity = 3 * triangle_capacity;
	triangles->positions = (float*)gl_primitive_alloc(triangles->vertex_capacity * 3 * sizeof(float));
	triangles->colors = (float*)gl_primitive_alloc(triangles->vertex_capacity * 4 * sizeof(float));
	triangles->normals = (float*)gl_primitive_alloc(triangles->vertex_capacity * 3 * sizeof(float));
	triangles->vertex_count = 0;

	glGenBuffers(3, triangles->vbos);

	glBindBuffer(GL_ARRAY_BUFFER, triangles->vbos[0]);
	glBufferData(GL_ARRAY_BUFFER, triangles->vertex_capacity * 3 * sizeof(float), triangles->positions, GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, triangles->vbos[1]);
	glBufferData(GL_ARRAY_BUFFER, triangles->vertex_capacity * 4 * sizeof(float), triangles->colors, GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, triangles->vbos[2]);
	glBufferData(GL_ARRAY_BUFFER, triangles->vertex_capacity * 3 * sizeof(float), triangles->normals, GL_DYNAMIC_DRAW);

	gl_primitive_assert_gl();

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	return triangles;
}

void gl_primitive_triangles_destroy(gl_primitive_triangles_t* triangles)
{
	glDeleteProgram(triangles->program);
	glDeleteBuffers(3, triangles->vbos);

	gl_primitive_free(triangles->positions);
	gl_primitive_free(triangles->colors);
	gl_primitive_free(triangles->normals);

	gl_primitive_free(triangles);
}

void gl_primitive_triangles_vertex(gl_primitive_triangles_t* triangles, float x, float y, float z, float r, float g, float b, float a, float nx, float ny, float nz)
{
	assert(triangles->vertex_count < triangles->vertex_capacity);
	triangles->positions[3 * triangles->vertex_count] = x;
	triangles->positions[3 * triangles->vertex_count + 1] = y;
	triangles->positions[3 * triangles->vertex_count + 2] = z;
	triangles->colors[4 * triangles->vertex_count] = r;
	triangles->colors[4 * triangles->vertex_count + 1] = g;
	triangles->colors[4 * triangles->vertex_count + 2] = b;
	triangles->colors[4 * triangles->vertex_count + 3] = a;
	triangles->normals[3 * triangles->vertex_count] = nx;
	triangles->normals[3 * triangles->vertex_count + 1] = ny;
	triangles->normals[3 * triangles->vertex_count + 2] = nz;
	++triangles->vertex_count;
}

void gl_primitive_triangles_flush(gl_primitive_triangles_t* triangles, float* mvp)
{
	if (triangles->vertex_count == 0)
	{
		return;
	}

	glUseProgram(triangles->program);

	glUniformMatrix4fv(triangles->projection_uniform, 1, GL_FALSE, mvp);

	glBindBuffer(GL_ARRAY_BUFFER, triangles->vbos[0]);
	glBufferSubData(GL_ARRAY_BUFFER, 0, triangles->vertex_count * 3 * sizeof(float), triangles->positions);
	glVertexAttribPointer(triangles->position_attribute, 3, GL_FLOAT, GL_FALSE, 0, gl_primitive_ptr_add(NULL, 0));
	glEnableVertexAttribArray(triangles->position_attribute);

	glBindBuffer(GL_ARRAY_BUFFER, triangles->vbos[1]);
	glBufferSubData(GL_ARRAY_BUFFER, 0, triangles->vertex_count * 4 * sizeof(float), triangles->colors);
	glVertexAttribPointer(triangles->color_attribute, 4, GL_FLOAT, GL_FALSE, 0, gl_primitive_ptr_add(NULL, 0));
	glEnableVertexAttribArray(triangles->color_attribute);

	glBindBuffer(GL_ARRAY_BUFFER, triangles->vbos[2]);
	glBufferSubData(GL_ARRAY_BUFFER, 0, triangles->vertex_count * 3 * sizeof(float), triangles->normals);
	glVertexAttribPointer(triangles->normal_attribute, 3, GL_FLOAT, GL_FALSE, 0, gl_primitive_ptr_add(NULL, 0));
	glEnableVertexAttribArray(triangles->normal_attribute);

	glDrawArrays(GL_TRIANGLES, 0, triangles->vertex_count);

	triangles->vertex_count = 0;

	gl_primitive_assert_gl();

	glDisableVertexAttribArray(triangles->position_attribute);
	glDisableVertexAttribArray(triangles->color_attribute);
	glDisableVertexAttribArray(triangles->normal_attribute);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glUseProgram(0);
}

#endif // #if !defined(gl_primitive_implementation_once)

#endif // #if defined(gl_primitive_implementation)
